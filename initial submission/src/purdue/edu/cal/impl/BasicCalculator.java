package purdue.edu.cal.impl;

import purdue.edu.cal.base.Calculator;

public abstract class  BasicCalculator  implements Calculator {

  @Override
  public double addition(double a, double b) {
    // TODO Auto-generated method stub
    
    return a + b;
  }

  @Override
  public double subtraction(double a, double b) {
    // TODO Auto-generated method stub
    return a - b;
  }

  @Override
  public double multiplication(double a,double b) {
    // TODO Auto-generated method stub
    return a * b;
  }

  @Override
  public double division(double a, double b) {
    // TODO Auto-generated method stub
    return a / b;
  }
  
  

}
